#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use]
extern crate rocket;

#[get("/")]
fn articles() -> &'static str {
    "articles"
}

#[get("/<a_id>")]
fn article(a_id: String) -> Option<String> {
    Some(format!("article id is {}", a_id))
}

#[post("/signup")]
fn signup() -> &'static str {
    "signup"
}

#[post("/login")]
fn login() -> &'static str {
    "login"
}

#[rocket::main]
async fn main() {
    let _ = rocket::ignite()
        .mount("/articles", routes![article, articles])
        .mount("/users", routes![login, signup])
        .launch()
        .await;
}
